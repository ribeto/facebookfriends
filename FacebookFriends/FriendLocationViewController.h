//
//  FriendLocatinoViewController.h
//  FacebookFriends
//
//  Created by Hristo Hristov on 12/11/12.
//  Copyright (c) 2012 Hristo Hristov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class User;

@interface FriendLocationViewController : UIViewController

@property (strong, nonatomic) User *friend;

@end

//
//  main.m
//  FacebookFriends
//
//  Created by Hristo Hristov on 12/10/12.
//  Copyright (c) 2012 Hristo Hristov. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
  }
}

//
//  FacebookManager.h
//  FacebookFriends
//
//  Created by Hristo Hristov on 12/11/12.
//  Copyright (c) 2012 Hristo Hristov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Accounts/Accounts.h>
#import <Social/Social.h>

@class Location;

@interface FacebookManager : NSObject

+ (FacebookManager *)sharedManager;

- (void)basicAuthenticationWithSuccess:(void(^)(ACAccount *facebookAccount))success andFailure:(void(^)(BOOL granted, NSError *error))failure;

- (void)checkinAuthenticationWithSuccess:(void(^)(ACAccount *facebookAccount))success andFailure:(void(^)(BOOL granted, NSError *error))failure;

- (void)loadFriendsDataWithAccount:(ACAccount *)facebookAccount withSuccess:(void(^)(NSArray *friends))success andFailure:(void(^)())failure;

- (void)loadCheckWithAccount:(ACAccount *)facebookAccount forUserId:(NSNumber *)userId withSuccess:(void(^)(NSArray *locations))success andFailure:(void(^)())failure;

@end

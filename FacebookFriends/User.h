//
//  User.h
//  FacebookFriends
//
//  Created by Hristo Hristov on 12/10/12.
//  Copyright (c) 2012 Hristo Hristov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property (strong, nonatomic) NSNumber  *userId;
@property (strong, nonatomic) NSString  *name;
@property (strong, nonatomic) NSURL     *pictureUrl;
@property (strong, nonatomic) UIImage   *picture;

+ (User *)newUserWithDictionary:(NSDictionary *)dictionary;

@end

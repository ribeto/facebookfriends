//
//  UserWallViewController.m
//  FacebookFriends
//
//  Created by Hristo Hristov on 12/11/12.
//  Copyright (c) 2012 Hristo Hristov. All rights reserved.
//

#import "UserWallViewController.h"
#import "FacebookManager.h"

@interface UserWallViewController ()

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation UserWallViewController


- (void)viewDidLoad
{
  [super viewDidLoad];
  
  __weak UserWallViewController *weakSelf = self;
  [[FacebookManager sharedManager] basicAuthenticationWithSuccess:^(ACAccount *facebookAccount) {
    UserWallViewController *strongSelf = weakSelf;
    
    if( strongSelf ) {
      // Do any additional setup after loading the view.
      NSURL *wallUrl = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.facebook.com/steven.lammertink?access_token=%@",facebookAccount.credential.oauthToken]];
      NSURLRequest *request = [NSURLRequest requestWithURL:wallUrl];
      [self.webView loadRequest:request];
    }
  } andFailure:^(BOOL granted, NSError *error) {
    
  }];
  
  

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)closeTapped:(id)sender {
  [self dismissViewControllerAnimated:YES completion:NULL];
}

@end

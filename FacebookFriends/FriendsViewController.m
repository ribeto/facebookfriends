//
//  FriendsViewController.m
//  FacebookFriends
//
//  Created by Hristo Hristov on 12/10/12.
//  Copyright (c) 2012 Hristo Hristov. All rights reserved.
//

#import "FriendsViewController.h"
#import "FriendLocationViewController.h"
#import "User.h"

@interface FriendsViewController ()


@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSOperationQueue *pictureQueue;

- (IBAction)postToWallTapped:(id)sender;
@end

@implementation FriendsViewController

static NSString * const kSegueShowMap = @"showMap";

- (id)initWithCoder:(NSCoder *)decoder {
  self = [super initWithCoder:decoder];
  if( self ) {
    self.pictureQueue = [NSOperationQueue new];
    [self.pictureQueue setMaxConcurrentOperationCount:4];
  }
  return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
  
  //
  // Stop loading images
  //
  [self.pictureQueue cancelAllOperations];
  
  //
  // Purge all current images
  //
  for( User *friend in self.friends ) {
    friend.picture = nil;
  }
  
  [self.tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated {
  
  // stop loading any images when the view goes away
  [self.pictureQueue cancelAllOperations];
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#pragma mark - UITableViewDataSource
////////////////////////////////////////////////////////////////////////////////
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  static NSString *Identifier = @"FriendCell";
  
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
  
  User *friend = self.friends[indexPath.row];
  
  // Usually subclass a UITableview Cell but for a simple exercise this will do
  UIImageView *imageView = (UIImageView *)[cell viewWithTag:101];
  UILabel *textLabel = (UILabel *)[cell viewWithTag:102];
  textLabel.text = friend.name;
  imageView.image = friend.picture;
  
  if( !friend.picture ) {
    
    __weak FriendsViewController *weakSelf = self;
    [self.pictureQueue addOperation:[NSBlockOperation blockOperationWithBlock:^{
      NSURLRequest *request = [NSURLRequest requestWithURL:friend.pictureUrl];
      NSURLResponse *response = nil;
      NSError *error = nil;
    
      NSData *picData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
      UIImage *pic = [UIImage imageWithData:picData];
      
      [[NSOperationQueue mainQueue] addOperation:[NSBlockOperation blockOperationWithBlock:^{
        
        FriendsViewController *strongSelf = weakSelf;
        
        if( strongSelf ) {
          friend.picture = pic;
          
          NSArray *cells = strongSelf.tableView.visibleCells;
          
          if( [cells count] > 0) {
            UITableViewCell *firstVisible = cells[0];
            UITableViewCell *lastVisible = cells[[cells count] - 1];
            NSIndexPath *firstIndexPath = [strongSelf.tableView indexPathForCell:firstVisible];
            NSIndexPath *lastIndexPath = [strongSelf.tableView indexPathForCell:lastVisible];
            
            // only update the cell if it's visible
            if( firstIndexPath.row <= indexPath.row && lastIndexPath.row >= indexPath.row ) {
              [strongSelf.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            }
          }
        }
      }]];
    }]];
  }
  
  return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return [self.friends count];
}

- (IBAction)postToWallTapped:(id)sender {
  SLComposeViewController *composeVC = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
  composeVC.completionHandler = ^(SLComposeViewControllerResult result) {

  };
  [self presentViewController:composeVC animated:YES completion:NULL];
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  if( [segue.identifier isEqualToString:kSegueShowMap] ) {
    NSIndexPath *selectedPath = [self.tableView indexPathForSelectedRow];
    User *friend = self.friends[selectedPath.row];
    [(FriendLocationViewController *)segue.destinationViewController setFriend:friend];
    [self.tableView deselectRowAtIndexPath:selectedPath animated:YES];
  }
}

- (void)dealloc {
  
}
@end

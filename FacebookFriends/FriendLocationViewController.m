//
//  FriendLocatinoViewController.m
//  FacebookFriends
//
//  Created by Hristo Hristov on 12/11/12.
//  Copyright (c) 2012 Hristo Hristov. All rights reserved.
//

#import "FriendLocationViewController.h"
#import "FacebookManager.h"
#import "User.h"
#import "Location.h"

@interface FriendLocationViewController ()
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end

@implementation FriendLocationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
  __weak FriendLocationViewController *weakSelf = self;
  [[FacebookManager sharedManager] checkinAuthenticationWithSuccess:^(ACAccount *facebookAccount) {
    FriendLocationViewController *strongSelf = weakSelf;
    [[FacebookManager sharedManager] loadCheckWithAccount:facebookAccount forUserId:self.friend.userId withSuccess:^(NSArray *locations) {
      
      if( strongSelf ) {
        if( [locations count] > 0 ) {
          Location *loc = locations[0];
          [self.mapView addAnnotation:loc];
          
          MKMapPoint annotationPoint = MKMapPointForCoordinate(loc.coordinate);
          MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0, 0);
          [strongSelf.mapView setVisibleMapRect:pointRect animated:YES];

        }
      }
      
    } andFailure:^{
      
    }];
  } andFailure:^(BOOL granted, NSError *error) {
    
  }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end

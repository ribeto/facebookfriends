//
//  ViewController.m
//  FacebookFriends
//
//  Created by Hristo Hristov on 12/10/12.
//  Copyright (c) 2012 Hristo Hristov. All rights reserved.
//

#import "LoginViewController.h"
#import "FriendsViewController.h"
#import "FacebookManager.h"
#import "User.h"

@interface LoginViewController ()

@property (weak, nonatomic) IBOutlet UIButton *facebookButton;

@property (strong, nonatomic) NSArray         *friends;

- (IBAction)facebookButtonTapped:(id)sender;

@end

@implementation LoginViewController

typedef enum {
  kFacebookNoAccountError = 6,
  kFacebookAccessDeniedError = 7
} t_FacebookError;

static NSString * const kSegueShowFriends   = @"showFriends";


- (void)viewDidLoad
{
  [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//
// Set the friends on the friends controller before pushing it
//
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  [self refreshUILoading:NO];
  
  if( [segue.identifier isEqualToString:kSegueShowFriends] ) {
    FriendsViewController *friendsVC = (FriendsViewController *)segue.destinationViewController;
    friendsVC.friends = self.friends;
    self.friends = nil;
  }
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#pragma mark - Alert
////////////////////////////////////////////////////////////////////////////////
- (void)showErrorWithMessage:(NSString *)message andTitle:(NSString *)title {
  dispatch_async(dispatch_get_main_queue(), ^{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    
    [self refreshUILoading:NO];
  });
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#pragma mark - Loading Friends
////////////////////////////////////////////////////////////////////////////////

- (void)loadFriendsDataWithAccount:(ACAccount *)facebookAccount {
   __weak LoginViewController *weakSelf = self;
  [[FacebookManager sharedManager] loadFriendsDataWithAccount:facebookAccount withSuccess:^(NSArray *friends) {
    LoginViewController *strongSelf = weakSelf;
    // check that we have not released the controller
    if( strongSelf ) {
      strongSelf.friends = friends;
      dispatch_async(dispatch_get_main_queue(), ^{
        //
        // Now that the friends are loaded and parsed we can show the friends controller
        //
        [self performSegueWithIdentifier:kSegueShowFriends sender:self];
      });
    }
  } andFailure:^{
    LoginViewController *strongSelf = weakSelf;
    // check that we have not released the controller
    if( strongSelf ) {
      [strongSelf showErrorWithMessage:@"Error loading data from the server" andTitle:@"Load Error"];
    }
  }];
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
- (void)refreshUILoading:(BOOL)isLoading {
  dispatch_async(dispatch_get_main_queue(), ^{
    self.facebookButton.enabled = !isLoading;
    if( isLoading ) {
      UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
      UIBarButtonItem *loadingButton = [[UIBarButtonItem alloc] initWithCustomView:spinner];
      self.navigationItem.rightBarButtonItem = loadingButton;
      [spinner startAnimating];
    } else {
      self.navigationItem.rightBarButtonItem = nil;
    }
  });
}


//
// Load the facebook account and the friends
//
- (IBAction)facebookButtonTapped:(id)sender {
  
  
  [self refreshUILoading:YES];
  
  __weak LoginViewController *weakSelf = self;
  [[FacebookManager sharedManager] basicAuthenticationWithSuccess:^(ACAccount *facebookAccount) {
    LoginViewController *strongSelf = weakSelf;
    // check that we have not released the controller
    if( strongSelf ) {
      [strongSelf loadFriendsDataWithAccount:facebookAccount];
    }
  } andFailure:^(BOOL granted, NSError *error) {
    
    LoginViewController *strongSelf = weakSelf;
    // check that we have not released the controller
    if( strongSelf ) {
      
      if( error.code == kFacebookNoAccountError ) {
        [strongSelf showErrorWithMessage:@"You'll need to login with Facebook in the Settings App"
                                andTitle:@"No Account"];
        
      } else if( !granted || error.code == kFacebookAccessDeniedError ) {
        [strongSelf showErrorWithMessage:@"Access Denied"
                                andTitle:@"Error"];
        
      }
      
    }
    
    
  }];
  
  
//  [self.accountStore requestAccessToAccountsWithType:facebookAccountType options:facebookOptions completion:^(BOOL granted, NSError *error) {
//    LoginViewController *strongSelf = weakSelf;
//    // check that we have not released the controller
//    if( strongSelf ) {
//      ACAccountStore *store = strongSelf.accountStore;
//      
//      //NSLog(@"error = %@",[error localizedDescription]);
//      if( granted && error == nil ) {
//
//          NSArray *accounts = [store accounts];
//          ACAccount *facebookAccount = [accounts lastObject];
//        
//          [strongSelf loadFriendsDataWithAccount:facebookAccount];
//        
//      } else if( error.code == kFacebookNoAccountError ) {
//        [strongSelf showErrorWithMessage:@"You'll need to login with Facebook in the Settings App"
//                          andTitle:@"No Account"];
//        
//      } else if( !granted || error.code == kFacebookAccessDeniedError ) {
//        [strongSelf showErrorWithMessage:@"Access Denied"
//                          andTitle:@"Error"];
//        
//      }
//    }
//    
//    
//  }];
  
}

@end

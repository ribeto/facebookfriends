//
//  UserWallViewController.h
//  FacebookFriends
//
//  Created by Hristo Hristov on 12/11/12.
//  Copyright (c) 2012 Hristo Hristov. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UserWallViewController : UIViewController <UIWebViewDelegate>

@end

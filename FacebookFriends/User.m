//
//  User.m
//  FacebookFriends
//
//  Created by Hristo Hristov on 12/10/12.
//  Copyright (c) 2012 Hristo Hristov. All rights reserved.
//

#import "User.h"

@implementation User

+ (User *)newUserWithDictionary:(NSDictionary *)dictionary {
  User *user = [User new];
  user.userId = dictionary[@"id"];
  user.name = dictionary[@"name"];
  user.pictureUrl = [NSURL URLWithString:dictionary[@"picture"][@"data"][@"url"]];
  
  return user;
}

@end

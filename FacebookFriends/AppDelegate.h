//
//  AppDelegate.h
//  FacebookFriends
//
//  Created by Hristo Hristov on 12/10/12.
//  Copyright (c) 2012 Hristo Hristov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

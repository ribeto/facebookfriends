//
//  Location.h
//  FacebookFriends
//
//  Created by Hristo Hristov on 12/11/12.
//  Copyright (c) 2012 Hristo Hristov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface Location : NSObject <MKAnnotation>

+ (Location *)newLocationWithDictionary:(NSDictionary *)locationDictionary;

@property (strong, nonatomic) NSNumber *latitude;
@property (strong, nonatomic) NSNumber *longitude;
@property (assign, nonatomic) CLLocationCoordinate2D coordinate;

@end

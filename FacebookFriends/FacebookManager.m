//
//  FacebookManager.m
//  FacebookFriends
//
//  Created by Hristo Hristov on 12/11/12.
//  Copyright (c) 2012 Hristo Hristov. All rights reserved.
//

#import "FacebookManager.h"
#import "User.h"
#import "Location.h"

@interface FacebookManager ()

@property (strong, nonatomic) ACAccountStore *accountStore;

@end

@implementation FacebookManager

static NSString * const kFacebookRootPath   = @"https://graph.facebook.com/";
static NSString * const kFacebookAppID      = @"272465656208890";

+ (FacebookManager *)sharedManager {
  static dispatch_once_t onceToken;
  static FacebookManager *_sharedInstance = nil;
  
  dispatch_once(&onceToken, ^{
    _sharedInstance = [FacebookManager new];
    _sharedInstance.accountStore = [ACAccountStore new];
  });
  
  return _sharedInstance;
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#pragma mark - Loading data from Facebook
////////////////////////////////////////////////////////////////////////////////

- (void)loadFromFacebookWithURL:(NSURL *)url andParameters:(NSDictionary *)parameters andAccount:(ACAccount *)facebookAccount withSuccess:(void(^)(NSData *responseData))success andFailure:(void(^)())failure {
  
  
  SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeFacebook
                                          requestMethod:SLRequestMethodGET
                                                    URL:url
                                             parameters:parameters];
  request.account = facebookAccount;

  [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
      
      // check that the fetch was successful
      if( error == nil ) {
        
        if( success != NULL ) success(responseData);
        
      } else {
        if( failure ) failure();
      }
  }];
}

- (void)loadFriendsDataWithAccount:(ACAccount *)facebookAccount withSuccess:(void(^)(NSArray *))success andFailure:(void(^)())failure {
  NSString *urlStr = [NSString stringWithFormat:@"%@me/friends", kFacebookRootPath];
  NSURL *url = [NSURL URLWithString:urlStr];
  [self loadFromFacebookWithURL:url
                  andParameters:@{ @"fields" : @"id,name,picture"}
                     andAccount:facebookAccount
                    withSuccess:^(NSData *responseData) {
                      
                      NSError *parseError = nil;
                      NSDictionary *friendsDict = [NSJSONSerialization JSONObjectWithData:responseData
                                                                                  options:NSJSONReadingMutableContainers
                                                                                    error:&parseError];
                      
                      // check that json parsing was successful
                      if( parseError == nil ) {
                        
                        NSMutableArray *friends = [NSMutableArray new];
                        
                        @autoreleasepool {
                          for( NSDictionary *friendDict in friendsDict[@"data"] ) {
                            User *friend = [User newUserWithDictionary:friendDict];
                            [friends addObject:friend];
                          }
                        }
                        
                        NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
                        NSArray *sortedFriends = [friends sortedArrayUsingDescriptors:@[descriptor]];
                        
                        if( success != NULL ) success(sortedFriends);
                        
                      } else if( failure != NULL ) {
                        
                        failure();
                        
                      }
                    }
                     andFailure:failure];
}

                
- (void)loadCheckWithAccount:(ACAccount *)facebookAccount forUserId:(NSNumber *)userId withSuccess:(void(^)(NSArray *locations))success andFailure:(void(^)())failure {
  NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/checkins",kFacebookRootPath,userId]];
  [self loadFromFacebookWithURL:url
                  andParameters:nil
                     andAccount:facebookAccount
                    withSuccess:^(NSData *responseData) {
                      
                      NSError *parseError = nil;
                      NSDictionary *checkinDict = [NSJSONSerialization JSONObjectWithData:responseData
                                                                                  options:NSJSONReadingMutableContainers
                                                                                    error:&parseError];
                      
                      // check that json parsing was successful
                      if( parseError == nil ) {
                        
                        NSMutableArray *locations = [NSMutableArray new];
                        for( NSDictionary *friendDict in checkinDict[@"data"] ) {
                          Location *loc = [Location newLocationWithDictionary:friendDict];
                          [locations addObject:loc];
                        }
                        
                        if( success != NULL ) success(locations);
                        
                      } else if( failure != NULL ) {
                        
                        failure();
                        
                      }
                    }
                     andFailure:failure];
}



////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#pragma mark - Authentication
////////////////////////////////////////////////////////////////////////////////

- (void)basicAuthenticationWithSuccess:(void(^)(ACAccount *))success andFailure:(void(^)(BOOL granted, NSError *error))failure {

  ACAccountType *facebookAccountType = [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
  
  NSDictionary *facebookOptions = @{
      ACFacebookAppIdKey : kFacebookAppID,
      ACFacebookPermissionsKey : @[@"email"],
      ACFacebookAudienceKey : ACFacebookAudienceOnlyMe
  };
  
  [self.accountStore requestAccessToAccountsWithType:facebookAccountType options:facebookOptions completion:^(BOOL granted, NSError *error) {

    ACAccountStore *store = self.accountStore;
    
    if( granted && error == nil ) {
      
      NSArray *accounts = [store accounts];
      ACAccount *facebookAccount = [accounts lastObject];
      
      if( success != NULL ) success(facebookAccount);
      
    } else if( failure != NULL ) {
      
      failure(granted, error);
      
    }
   
   
  }];
}

- (void)checkinAuthenticationWithSuccess:(void(^)(ACAccount *facebookAccount))success andFailure:(void(^)(BOOL granted, NSError *error))failure {
  ACAccountType *facebookAccountType = [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
  
  NSDictionary *facebookOptions = @{
  ACFacebookAppIdKey : kFacebookAppID,
  ACFacebookPermissionsKey : @[@"friends_checkins"],
  ACFacebookAudienceKey : ACFacebookAudienceOnlyMe
  };
  
  [self.accountStore requestAccessToAccountsWithType:facebookAccountType options:facebookOptions completion:^(BOOL granted, NSError *error) {
    
    ACAccountStore *store = self.accountStore;
    
    if( granted && error == nil ) {
      
      NSArray *accounts = [store accounts];
      ACAccount *facebookAccount = [accounts lastObject];
      
      if( success != NULL ) success(facebookAccount);
      
    } else if( failure != NULL ) {
      
      failure(granted, error);
      
    }
    
    
  }];
}

@end

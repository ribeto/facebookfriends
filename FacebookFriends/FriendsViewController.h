//
//  FriendsViewController.h
//  FacebookFriends
//
//  Created by Hristo Hristov on 12/10/12.
//  Copyright (c) 2012 Hristo Hristov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>

@interface FriendsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSArray *friends;

@end

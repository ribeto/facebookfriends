//
//  Location.m
//  FacebookFriends
//
//  Created by Hristo Hristov on 12/11/12.
//  Copyright (c) 2012 Hristo Hristov. All rights reserved.
//

#import "Location.h"

@implementation Location

+ (Location *)newLocationWithDictionary:(NSDictionary *)locationDictionary {
  Location *loc = [Location new];
  loc.latitude = [NSNumber numberWithDouble:[locationDictionary[@"place"][@"location"][@"latitude"] doubleValue]];
  loc.longitude = [NSNumber numberWithDouble:[locationDictionary[@"place"][@"location"][@"longitude"] doubleValue]];
  
  loc.coordinate = CLLocationCoordinate2DMake([loc.latitude doubleValue], [loc.longitude doubleValue]);
  
  return loc;
}

@end
